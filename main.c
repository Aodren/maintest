/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 20:25:38 by abary             #+#    #+#             */
/*   Updated: 2016/02/21 21:54:29 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h> /* for fork */
#include <sys/types.h> /* for pid_t */
#include <sys/wait.h> /* for wait */
#include "libft.h"

// On suppose a<b
//
int rand_a_b(int a, int b){
     return rand()%(b-a) +a;
}

int		main(int argc, char **argv)
{
	int nbr = 10, len = -1, i = 10, swap, index;
	int *tab;
	srand(time(NULL));

	if (argc > 1)
	{
		nbr = atoi(argv[1]);
		i = -nbr / 2;
	}
	tab = malloc(sizeof(int) * nbr);

	while (++len < nbr)
		tab[len] = i++;
	len = -1;

	while (++len < nbr)
	{
		index = rand_a_b(len, nbr);
		swap = tab[len];
		tab[len] = tab[index];
		tab[index] = swap;
	}
	len = -1;

	//popen("./push");
	//
	/*Spawn a child to run the program.*/
	pid_t pid=fork();
	if (pid==0) { /* child process */
		static char **argv;

		argv = malloc(sizeof(char **) * nbr + 1);
		int lenlen = -1;
		while ( ++lenlen < nbr)
		{

			argv[lenlen] = ft_itoa(tab[lenlen]);
		}
		argv[lenlen] = NULL;
		execv("./push_swap",argv);
		exit(127); /* only if execv fails */
	}
	else { /* pid!=0; parent process */
		waitpid(pid,0,0); /* wait for child to exit */
	}
	return (1);
}
